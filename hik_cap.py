
import cv2
from hikvision import *


#cap = cv2.VideoCapture(0)
cap = VideoCaptureHIK(0)


while(True):
    ret, frame = cap.read()
    frame = cv2.resize(frame,(612,512))
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        out = cv2.imwrite(r'C:\Users\BlinkDEV11\Desktop\capture.jpg', frame)
        break

cap.release()
cv2.destroyAllWindows()
